{
    'name': 'To-Do application',
    'description': 'Manage your personal tasks',
    'author': 'Dennis Decoene',
    'depends': ['mail'],
    'application': True,
    'data': ['todo_view.xml',
             'security/ir.model.access.csv',
             'security/todo_access_rules.xml']
}
